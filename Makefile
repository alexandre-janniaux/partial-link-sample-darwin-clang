CC = clang

all: libplugin.dylib

libcontrib.a: contrib.o contrib_internal.o
	$(AR) cru $@ $^ 

plugin.partial.o: LDFLAGS="-r"
plugin.partial.o: plugin.o libcontrib.a
	$(LINK.o) -o $@ $^

libplugin.dylib: plugin.partial.o
	$(LINK.o) -shared -o $@ $^
