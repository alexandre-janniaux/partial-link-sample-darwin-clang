int InitVideoDec(void);

__attribute__((visibility("default")))
int vlc_entry__avcodec() {
    if (InitVideoDec())
        return 1;
    return 0;
}
